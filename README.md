## About Project

This is an Assessment tool developed for the VMware team. This is a question/answer type tool in the end you will get the result based of the given answers. Dynamic PDF generations and the form for lead capture is also present on the tool.

---

## Folder Structure

1. **css** this folder contains all the CSS files
2. **images** this folder contains all the images
3. **js** this folder contains all the JS files
4. **index.html** this is the main index file

---

## Technologies Used

1. HTML5
2. CSS3
3. Javascript
4. jQuery
5. jsPDF

## Please keep in mind while deploying files on server

/index.html - x_api_key is different for stage & production
/js/script.js - API endpoint URL is different for stage & production