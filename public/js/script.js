var totalAns, totalQues, calLen, oldColVal, totalFeilds,selectVal = true, mailVal = false, checkInputs = false, fillNum, fillLen, totalSum, windowHeight, sectionVal, backClick = false, filterApplied = false;
var sumVar = [];
var sumlen = [];
var sumPrty = [0,0,0,0,0];
var activeFilters = [];
var removedElem, prevElem, hideSection;
var hideSectionArray = [0,0,0,0,0];
var hideBlock = [0,0,0,0,0];
var elemPosition = [0,0,0,0,0];
var calcNa = [0,0,0,0,0]; 
var backArr, barWidth, barWidthVal, totalFieldBox, $emptyField;
var score_details = {};
function reset_assessment(){
    $('.main-wrapper .circle').removeClass('active');
    $('.progress-bar .fill').width('0%');
    $('.progress-bar .fill .handle p').text('0%');
    $('.main-wrapper .fill').width('0%');
    $('.main-wrapper .fill-diff').width('0%');
    $('.main-wrapper .btn.next').removeClass('active');
    $('.question-content h4').removeClass('active');
    $('#subForm')[0].reset();
    filterApplied = false;
}
function answer_length(section){
    var answer_len = 0;
    $('.'+section+'.main-wrapper .question-content').each(function(i, n) { 
        var curr_answer_len = $(this).find('.circle.active').length;
        if(parseInt(curr_answer_len) >= 1){
            answer_len +=1;
        }
    });//console.log(answer_len);
   return answer_len;
}
function progressbar(current_section){ 
    var section_array = ['decSection', 'filter', 'network', 'branch', 'automation', 'multi-cloud', 'cloud'];
    var total_percentage = 0;
    var no_of_section = 0;
    for( key in sumPrty ){ 
        if(sumPrty[key] > 0){
            no_of_section++;
        }
    }   
    var single_question_val = 0;
    if(no_of_section>0){ 
        single_question_val = (60 / no_of_section) / 4;
    }
    for(section in section_array){
        if(section_array[section] == "decSection"){ 
            var section_len = $('.'+section_array[section]+'.main-wrapper .circle.active').length; 
            total_percentage += parseInt(section_len) * 5; 
        }else if(section_array[section] == "filter"){ 
             var filter_section_len =  $('.filter.main-wrapper h4.active').length;
             total_percentage += ( parseInt(filter_section_len) >= 1 )? 5: 0; 
        }else{ 
            // var section_len = $('.'+section_array[section]+'.main-wrapper .circle.active').length;
            var section_len = answer_length(section_array[section]);
            total_percentage += parseInt(section_len) * single_question_val; 
        }
        // if(section_array[section] == current_section){
        //     break;
        // }
    }       
    if( (total_percentage >= 0) && (total_percentage <= 100) ){
        $('.progress-bar .fill').width(Math.round(total_percentage)+'%');
        $('.progress-bar .fill .handle p').text(Math.round(total_percentage)+'%');
    }
    //console.log(total_percentage+ " total_percentage | single_question_val "+single_question_val );    
}
var assessment_start_at = 0;
var assessment_end_at = 0;
var session_id = localStorage.getItem("session_id");
var user_id = localStorage.getItem("user_id");
function pushEvent(api_input){ 
   EventApi.push(api_input);
}

function trackEvent(section_selector){  
    var current_section = section_selector.attr('data-id');
    // console.log(current_section);
    var api_input;
    if(current_section =='decSection'){
        var priorities_submission = {};
        var answer_selector = jQuery('.'+current_section+' .question-wrapper .cat-title:first .title-parts');
        jQuery('.'+current_section+' .question-content').each(function() {
            var currentElement = $(this);
            // var question = currentElement.find('.title h3').text();
            var question = currentElement.attr('data-section'); 
            var answer_index = currentElement.find('.cat-title .title-parts .active').parent('.title-parts').index();
            var answer = answer_selector.eq(answer_index).text();     
            priorities_submission[question] = answer.trim();
        });
        api_input = {
                'data':priorities_submission,
                'type': 'profile'
            };
    }else if(current_section =='filter'){
        var primary_obstacles = [];
        jQuery('.'+current_section+' .question-content h4.active').each(function() {
            var currentElement = $(this);
            primary_obstacles.push(currentElement.text());
        });
        api_input = {
                'data': primary_obstacles,
                'type': 'obstacles'
            };        
    }else if(current_section =='form'){ 
        assessment_end_at = (new Date()).getTime();
        var duration = assessment_end_at - assessment_start_at;
        api_input = {
            'data':{
                'duration': duration
            },
            'type': 'complete'
        };
    }else{
        if(section_selector.hasClass('field')){
            var assessment_at_section_level = {};
            var section_name = jQuery('.'+current_section+' .question-wrapper .title h1').text();
            jQuery('.'+current_section+' .question-content').each(function() { 
                var currentElement = $(this);
                var question = currentElement.find('.title h3').text();
                if( $(this).hasClass('multiselect') ){
                    var answer_array = [];
                    currentElement.find('.circle-wrapper .circle.active').each(function(){
                        var answer = $(this).parent('.circle-container').text();
                        answer_array.push(answer.replace(/\s\s+/g, ' ').trim());
                    });
                    assessment_at_section_level[question] = answer_array;
                }else{
                    var answer = currentElement.find('.circle-wrapper .circle.active').parent('.circle-container').text();
                    assessment_at_section_level[question] = answer.replace(/\s\s+/g, ' ').trim();                    
                }               
                
            });
            api_input = {
                    'name':section_name,
                    'data':assessment_at_section_level,
                    'type': 'assessment'
                };            
        }
    }
    if(api_input != undefined){
        pushEvent(api_input);
    }
}
function launchApp(){
    var api_input = {
            'data':{
                'referer': document.referrer
            },
            'type': 'visit'
        };
    pushEvent(api_input);

}
function startAssessment(){
    var api_input = {
            'type': 'start'
        };
    pushEvent(api_input);
}
function assessmentScore(score_details){
    var api_input = {
            'data': score_details,
            'type': 'capability'
        };
    pushEvent(api_input);
}
function track_event(){
    $(".track-event").click(function(){
        var data_name = $(this).attr('data-name');
        if(data_name == undefined){
            data_name = $(this).text();
        }
        var api_input = {
            'name': data_name,
            'type': $(this).attr('data-type'),
        };
        pushEvent(api_input);
    });
    
}
function onlyAlphabets(e, t) { //console.log(t.value);
        try {
            // console.log((t.value).replace(/\s\s+/g, ' '));
            $(t).val((t.value).replace(/\s\s+/g, ' '));
            if (window.event) {
                var charCode = window.event.keyCode;
            }
            else if (e) {
                var charCode = e.which;
            }
            else { return true; }
            if ((charCode > 64 && charCode < 91) || (charCode > 96 && charCode < 123) || (charCode==32) )
                return true;
            else
                return false;
        }catch (err) {
            //console.log(err.Description);
        }

}
 function IsAlphaNumeric(e, t) {
      $(t).val((t.value).replace(/\s\s+/g, ' '));
       var keyCode = e.keyCode == 0 ? e.charCode : e.keyCode;
       var ret = ((keyCode >= 48 && keyCode <= 57) || (keyCode >= 65 && keyCode <= 90) || (keyCode >= 97 && keyCode <= 122) || (keyCode == 32));
       // document.getElementById("error").style.display = ret ? "none" : "inline";
       return ret;
   }

function IsNumberic(e, t) { 
    var len = (t.value).length; 
    if( (e.keyCode != 46) && (e.keyCode != 101) && (e.keyCode != 45) && (parseInt(len) <= 9 ) ){
    return true;
    }else{
    return false;
    }
}

$(document).ready(function(){
    $('.footer-content .year').text((new Date()).getFullYear());
    //launchApp();
    // populateCountries('country', 'state');

    $('.show-na-alert .okay-btn').on('click', function(){
        $(this).parents('.show-na-alert').hide();
        // calcNa = [0,0,0,0,0];
    });
    $('.form-validation-alert .okay-btn').on('click', function(){
        $(this).parents('.form-validation-alert').hide();
        // calcNa = [0,0,0,0,0];
    });


// Begin btn
   $('.next-pg-cta').on('click', function(){
        $(this).parents('.container').removeClass('active');
        $(this).parents('.container').next('.container').addClass('active');
        $('.container.active').find('.main-wrapper').eq(0).addClass('active');
        // fillUpdate();
        startAssessment();
        assessment_start_at = (new Date()).getTime();
   });



   $('body').on('click', '.circle', function(e){



       if($(this).hasClass('active')){
            if( $(this).parents('.question-content').hasClass("multiselect") ){
                $(this).removeClass('active');
            }
            progressbar('');

            totalQues = $(this).parents('.main-wrapper.active').find('.question-content').length;
            totalAns = answer_length($(this).parents('.main-wrapper.active').attr('data-id'));
            // console.log(totalQues+ " totalQues");
            // console.log(totalAns+ " totalAns");
            if(totalQues == totalAns){
                $(this).parents('.main-wrapper').find('.btn.next').addClass('active');
            }else{
                $('.btn.next').removeClass('active');
            }

            // if(!$(this).parents('.main-wrapper').hasClass('empty')){
            //     var ansVal = 0;
            //     if( $(this).parents('.question-content').hasClass("multiselect") ){
            //        $(this).parents('.question-content').find('.circle.active').each(function(i, n) {
            //            ansVal += 1;
            //         });
            //     }else{
            //         ansVal = $(this).parent('.circle-container').index()+1;
            //     }                
            //     var colVal = $(this).parents('.question-content').index();
            //     console.log(ansVal+" ansVal");
            //     console.log(colVal+" colVal");
            //     sumVar[colVal] = ansVal;
            // }

           e.preventDefault();
       } else{
        if( !$(this).parents('.question-content').hasClass("multiselect") ){
             $(this).parents('.question-content').find('.circle').removeClass('active');
        }
       
        $(this).addClass('active');
                totalQues = $(this).parents('.main-wrapper.active').find('.question-content').length;
                totalAns = answer_length($(this).parents('.main-wrapper.active').attr('data-id'));

                // totalAns = $(this).parents('.main-wrapper.active').find('.circle.active').length;
             // console.log(totalQues+ " totalQues");
             // console.log(totalAns+ " totalAns");
            if(totalQues == totalAns){
                $(this).parents('.main-wrapper').find('.btn.next').addClass('active');
            }
            else{
                $('.btn.next').removeClass('active');
            }
            if(!$(this).parents('.main-wrapper').hasClass('empty')){

            // var ansVal = 0;
            // if( $(this).parents('.question-content').hasClass("multiselect") ){
            //    $(this).parents('.question-content').find('.circle.active').each(function(i, n) { 
            //        ansVal += 1;
            //     });

            // }else{
            //     ansVal = $(this).parent('.circle-container').index()+1;
            // }
            
            // var colVal = $(this).parents('.question-content').index();
            // console.log(ansVal+" ansVal");
            // console.log(colVal+" colVal");
 
        $('.main-wrapper.field').each(function(){
           a = $('.main-wrapper.field').length;
           
        });
            
           // if($(this).parents('.main-wrapper').find('.circle.active').length <= 4){
        
              // var c = $(this).parents('.main-wrapper').find('.circle.active').length;
            
      
                // $(this).parents('.container').find('.progress-bar .fill').width((parseInt(barWidthVal[0]))+(2*c)+'%');
                //  $(this).parents('.container').find('.progress-bar .fill .handle p').text((parseInt(barWidthVal[0]))+(2*c)+'%');
               // }
            // if( backClick == true){
            //     sumVar = backArr;
            // }
           // sumVar[colVal] = ansVal;
            
           // console.log("sumVar");
           // console.log(sumVar);
            // totalSum = sumVar.reduce(getSum);
            // console.log("totalSum "+totalSum);
            // calLen = (totalSum/20)*100;
            // console.log("calLen "+calLen);
           
            if($(this).parents('.main-wrapper').find('.circle.active').length == 4){
                checkInputs = true;
            }
      
        }
        else{   //console.log("test1111111111");       console.log('.main-wrapper.'+$(this).parents('.question-content').attr('data-section')+' .circle');       
            $('.main-wrapper.'+$(this).parents('.question-content').attr('data-section')+' .circle').removeClass('active');
            $('.main-wrapper.'+$(this).parents('.question-content').attr('data-section')+' .btn.next').removeClass('active');
            

            // if($(this).parents('.main-wrapper').find('.circle.active').length <= 5){
            //     var a = $(this).parents('.main-wrapper').find('.circle.active').length;
            //      $(this).parents('.container').find('.progress-bar .fill').width(5*a+'%');
            //      $(this).parents('.container').find('.progress-bar .fill .handle p').text(5*a+'%');
            //  }
            var ansVal = $(this).parent('.title-parts').index()+1;
            var colVal = $(this).parents('.question-content').index();

            if(ansVal == 1){
                $(this).parents('.question-content').each(function(){
                    calcNa[colVal] = ansVal;
                    // console.log(calcNa);
                    var a;
                    a = calcNa.reduce(getSum);
                    // console.log(a);
                    
                    if(a == 5){
                        $('.show-na-alert').show();
                            $(this).parents('.main-wrapper').find('.btn.next').removeClass('active');
                            $(this).find('.circle').removeClass('active');
                            checkInputs = false;
                    }
                    // else{
                    //     $('.show-na-alert').hide();
                    //             $(this).parents('.main-wrapper').find('.btn.next').addClass('active');
                    //             checkInputs = true;
                    // }
                });
                ansVal = 0;
                var b = $(this).parents('.question-content').index();
                oldColVal = colVal;
                hideSection = $(this).parents('.question-content').attr('data-section');
                prevElem = $('.main-wrapper.'+hideSection+'').attr('data-sort');
                // console.log(prevElem);
                // console.log(a);
                removedElem  = $('.main-wrapper.'+hideSection+'');
                hideBlock[b] = removedElem;
                // console.log(hideBlock);
                hideSectionArray[b] = hideSection;
                // console.log(hideSectionArray);
                // for(var z=0;z<hideSectionArray.length;z++){
                //     if(hideSectionArray[z] == 0){
                //         $('.show-na-alert').show();
                //         $(this).parents('.main-wrapper').find('.btn.next').removeClass('active');
                //         checkInputs = false;
                //     }
                //     else{
                //         $('.show-na-alert').hide();
                //         $(this).parents('.main-wrapper').find('.btn.next').addClass('active');
                //         checkInputs = true;
                //     }
                // }
               
               $('.main-wrapper.'+hideSection+'').remove();
            }
            else{
                calcNa[colVal] = 0;
                var b = $(this).parents('.question-content').index();
                // if(oldColVal == colVal){
                $(hideBlock[b]).insertAfter($('.main-wrapper.filter'));
                $(".main-wrapper.field").sort(sort_li) // sort elements
                .insertAfter($('.main-wrapper.filter')); // append again to the list
                hideSectionArray[b] = 0;
            // }
            }
            
           
            $(this).parents('.main-wrapper').find('.score-wrapper').eq(colVal).find('.bar .fill').width((ansVal - 1)*25+'%');
            sumPrty[colVal] = ansVal;
            if($(this).parents('.main-wrapper').find('.circle.active').length == 5){
                checkInputs = true;
            }
                    
     
        }
     
        var section_name = $(this).parents('.main-wrapper').attr('data-id');
        progressbar(section_name);

    }


   });
   $('body').on('click', '.main-wrapper.field .block-level .question-wrapper .circle-container p', function(){
        $(this).parents('.circle-container').find('.circle').trigger('click');
   });
   $('.main-wrapper .block-level .question-wrapper .question-wrapper-content .question-content h4').on('click', function(e){

       if(e.target.tagName != 'SPAN'){
         $(this).addClass('active');
       }
       else{
        $(this).removeClass('active');
       }
       $('.main-wrapper .block-level .question-wrapper .question-wrapper-content .question-content h4').each(function(){
        // if($('.main-wrapper .block-level .question-wrapper .question-wrapper-content .question-content h4.active').length > 0 && filterApplied == false){
        //     $(this).parents('.container').find('.progress-bar .fill').width(parseInt(barWidthVal[0])+8+'%');
        //     $(this).parents('.container').find('.progress-bar .fill .handle p').text(parseInt(barWidthVal[0])+8+'%');
        // }
            if($('.main-wrapper .block-level .question-wrapper .question-wrapper-content .question-content h4.active').length > 0){
                $(this).parents('.main-wrapper').find('.btn.next').addClass('active');
                checkInputs = true;
            }
            else{
                // $(this).parents('.container').find('.progress-bar .fill').width((barWidthVal[0])+'%');
                // $(this).parents('.container').find('.progress-bar .fill .handle p').text((barWidthVal[0])+'%');
                $(this).parents('.main-wrapper').find('.btn.next').removeClass('active');
                checkInputs = false;
            }

        var section_name = $(this).parents('.main-wrapper').attr('data-id');
        progressbar(section_name);

       });
   });

   $(document).on('click', '.btn.next', function(e){
    if($(this).parents('.main-wrapper').hasClass('form')){
   $('form input').each(function(){
        if($(this).val() == ''){
            $(this).css('border','1px solid red');
        }
     });
   $('form select').each(function(){
        if($(this).val() == '' || $(this).val() == '-1'){
            $(this).css('border','1px solid red');
        }
     });

   if($(this).parents('.main-wrapper').find('.btn.next').hasClass('active')){
    
   }else{
    $('.form-validation-alert').show();
   }
 
   
}



});

   $(document).on('click', '.btn.next.active', function(e){
    trackEvent($(this).parents('.main-wrapper'));





    if($(this).parents('.main-wrapper').next('.filter').find('.question-content h4.active').length > 0){
       //  var a =  $(this).parents('.main-wrapper').next('.filter').attr('data-progress');
       // $(this).parents('.container').find('.progress-bar .fill').width(a);
       // $(this).parents('.container').find('.progress-bar .fill .handle p').text(a);
       filterApplied = true;
   }

    if($(this).parents('.main-wrapper').hasClass('decSection')){
        
        $('.main-wrapper.field').each(function(){
            totalFieldBox = $('.main-wrapper.field').length;
        
      });
      for(var z=0;z<totalFieldBox;z++){
        sumVar.push(0);
    }
    }

    if(!$(this).parents('.main-wrapper').hasClass('empty') && !$(this).parents('.main-wrapper').hasClass('form')){

        var answer_len = answer_length($(this).parents('.main-wrapper.field').attr('data-id'));
        // console.log(answer_len + " answer_len");
        if(answer_len == 4){
            checkInputs = true;
            $(this).parents('.main-wrapper.field').find('.question-content').each(function(){

                var ansVal = 0;
                if( $(this).hasClass("multiselect") ){
                   $(this).find('.circle.active').each(function(i, n) { 
                       ansVal += 1;
                    });

                }else{
                    ansVal = $(this).find('.circle.active').parent('.circle-container').index()+1;
                }                
                var colVal = $(this).index();
                // console.log(colVal+" colVal " + ansVal+ " ansVal");

                // var ansVal = $(this).parents('.circle-container').index()+1;
                // var colVal = $(this).parents('.question-content').index();
                sumVar[colVal] = ansVal;
   
                totalSum = sumVar.reduce(getSum);
                // console.log(totalSum + " totalSum");
                calLen = (totalSum/20)*100;
                // console.log(calLen + " calLen");
            });
           // console.log($(this).parents('.main-wrapper.field').attr('data-id') + '  '+ totalSum + ' '+ calLen);

        }
    }


    // barWidth = $(this).parents('.container').find('.progress-bar .fill .handle p').text();
    // barWidthVal = barWidth.split('%');


    if($(this).parents('.main-wrapper').hasClass('filter')){
        if($('.filter .question-content h4').hasClass('active')){
            checkInputs = true;
        }
    }
if(checkInputs == true){


    if(!$(this).parents('.main-wrapper').hasClass('empty') && !$(this).parents('.main-wrapper').hasClass('form')){
        var colName = $(this).parents('.main-wrapper').attr('data-id');
        sectionVal = colName+'.'+calLen;
    
        var f = $(this).parents('.main-wrapper.field').index()-3;
        sumlen[f] = sectionVal;

 
        // $(this).parents('.container').find('.progress-bar .fill').width(parseInt((parseInt(barWidthVal[0]))+(8*(5-a)/a))+'%');
        // $(this).parents('.container').find('.progress-bar .fill .handle p').text(parseInt((parseInt(barWidthVal[0]))+(8*(5-a)/a))+'%');
    }
   // if($(this).parents('.main-wrapper').next('.field').find('.circle.active').length == 4){
        // var a;
        // $('.main-wrapper.field').each(function(){
        //    a = $('.main-wrapper.field').length;
           
        // });
        // var a = $(this).parents('.main-wrapper.field').find('.circle.active').length;
        // barWidth = $(this).parents('.container').find('.progress-bar .fill .handle p').text();
        // barWidthVal = barWidth.split('%');
        // $(this).parents('.container').find('.progress-bar .fill').width(parseInt(barWidthVal)+(2*a)+'%');
        // $(this).parents('.container').find('.progress-bar .fill .handle p').text(parseInt(barWidthVal)+(2*a)+'%');
   // }

    if($(this).parents('.main-wrapper').next().hasClass('form')){
        // $(this).parents('.container').find('.progress-bar .fill').width('90%');
        // $(this).parents('.container').find('.progress-bar .fill .handle p').text('90%');

       $('.main-wrapper.form .score-wrapper').each(function(){
        hideResultBar(this);
        var c;
        var a = $(this).attr('data-id'); //console.log(sumlen);
        for(var y = 0;y<sumlen.length;y++){
            var b = sumlen[y];
             c = b.split(".");
            if(a == c[0]){
                $(this).find('.bar .fill-diff').width(c[1]+'%');
            }
        }
      
        fillNum = $(this).index();
        //  fillLen = sumlen[fillNum];
         fillLenNew = sumPrty[fillNum];
   
       $(this).find('.bar .fill').width((fillLenNew - 1) *25+'%');
  
       });
    }
    // sumVar = [];

    if($(this).parents('.main-wrapper').next().hasClass('empty')){
       var prevRes =  $(this).parents('.main-wrapper').find('.block-level.md').html();
       $(this).parents('.main-wrapper').next().find('.block-level.md').html(prevRes);
    }
    if($(this).parents('.main-wrapper').next('.main-wrapper').length){
        $(this).parents('.main-wrapper').removeClass('active');
        $(this).parents('.main-wrapper').next('.main-wrapper').addClass('active');
        // fillUpdate();
    }
    if($(this).parents('.main-wrapper').next('.main-wrapper').hasClass('field')){
        $('.progress-bar').css('margin','auto');

        
            var total_q = $(this).parents('.main-wrapper').next('.main-wrapper').find('.question-content').length;
            // var total_a = $(this).parents('.main-wrapper').next('.main-wrapper').find('.circle.active').length;
            var total_a = answer_length($(this).parents('.main-wrapper').next('.main-wrapper').attr('data-id'));
            if( (parseInt(total_q) >= 1) && (total_q == total_a) ){
                $(this).parents('.main-wrapper').next('.main-wrapper').find('.btn.next').addClass('active');
            }else{
                 $(this).parents('.main-wrapper').next('.main-wrapper').find('.btn.next').removeClass('active');
            }
      



        totalFeilds = $('.main-wrapper.field').length;
    //     for(var a=0;a<totalFeilds;a++){
    //     sumVar.push(0);
    // }
    } else{
        $('.progress-bar').css('margin',' 0.25vw 0 3vw');
    }
    if($(this).parents('.main-wrapper').hasClass('filter')){
        $('.filter .question-content h4.active').each(function(){
            var appliedFilter = $(this).parents('.question-content').index();
            activeFilters.push(appliedFilter);
        });
        // var a = $(this).parents('.main-wrapper').next('.field').find('.circle.active').length;
        // barWidth = $(this).parents('.container').find('.progress-bar .fill .handle p').text();
        // barWidthVal = barWidth.split('%');
        // $(this).parents('.container').find('.progress-bar .fill').width(parseInt(barWidthVal)+(2*a)+'%');
        // $(this).parents('.container').find('.progress-bar .fill .handle p').text(parseInt(barWidthVal)+(2*a)+'%');
    }
    if($(this).parents('.main-wrapper').hasClass('form')){
        $('#subForm').submit(function(e) {
            e.preventDefault();
        });   
        $('.loder_show_after_form_submit').show();
        var form_data = $('#subForm').serialize();
        $.ajax({
            type: "POST",
            url: "https://s279193683.t.eloqua.com/e/f2", // Production
            // url: "demo-text.txt", // Stage
            data:form_data,
            success: function(response) {
                $('.loder_show_after_form_submit').hide();
                getKenshooScript();
              
                $('.container').removeClass('active');
                $('.container.result-container').addClass('active');
                $('.result-score .score-wrapper').each(function(){
                    hideResultBar(this);
                    //     fillNum = $(this).index();
                    //      fillLen = sumlen[fillNum];
                    //    $(this).find('.bar .fill-diff').width(fillLen+'%');
                    var c;
                    var a = $(this).attr('data-id'); 
                    score_details[a]=0;
                    for(var y = 0;y<sumlen.length;y++){
                        var b = sumlen[y];
                         c = b.split(".");
                        if(a == c[0]){
                            $(this).find('.bar .fill-diff').width(c[1]+'%');
                            $(this).find('.bar .fill-diff').attr('data-width', c[1]);
                            if(!fillLen){
                                fillLen = 0;
                            }
                               $(this).find('p.final-results').text((c[1]/100*20)+'/20');
                               score_details[a]=c[1]/100*20;
                        }
                        
                    } 
            
                       });
                       $('.chart .score-wrapper').each(function(){
                        hideResultBar(this);
                        fillNum = $(this).index();
                        var c;
                        var a = $(this).attr('data-id');
                        for(var y = 0;y<sumlen.length;y++){
                            var b = sumlen[y];
                             c = b.split(".");
                            if(a == c[0]){
                                $(this).find('.bar .fill-diff').width(c[1]+'%');
                                $(this).find('.bar .fill-diff').attr('data-width', c[1]);
                                
                            }
                            
                        }
                        //  fillLen = sumlen[fillNum];
                         fillLenNew = sumPrty[fillNum];
                    //    $(this).find('.bar .fill-diff').width(fillLen+'%');
                       $(this).find('.bar .fill').width((fillLenNew-1)*25+'%');
                       $(this).find('.bar .fill').attr('data-width', (fillLenNew - 1) *25);
                       });
                    //    console.log(activeFilters.length);
                       for(var i = 0; i<activeFilters.length;i++){
                            $('.budget-wrapper .two-block').eq(activeFilters[i]).find('.budget-list-wrapper').addClass('active');
                       }
                       $('.summary .two-block-wrapper').each(function(){
                
                        var c;
                        var a = $(this).attr('id');
                        for(var y = 0;y<sumlen.length;y++){
                            var b = sumlen[y];
                             c = b.split(".");
                            if(a == c[0]){
                                $(this).find('.bar .fill-diff').width(c[1]+'%');
                                $(this).find('.bar .fill-diff').attr('data-width', c[1]);
                                if(!fillLen){
                                    fillLen = 0;
                                }
                                   $(this).find('p.final-results').text((c[1]/100*20)+'/20');

                           
                            }
                          
                            
                        }
                  
                        var x = $(this).find('p.final-results').text();
                        if(x!="N/A"){
                            var y = x.split("/");
                            if(y[0] <= 6){
                                $(this).find('li').eq(2).addClass('active');
                            }
                            else if(y[0] > 6 && y[0]<= 13){
                                $(this).find('li').eq(1).addClass('active');
                            }
                            else{
                                $(this).find('li').eq(0).addClass('active');
                            }

                        }
                       });
                    //    for(var a=0;a<hideSectionArray.length;a++){
                    //     $('#'+hideSectionArray[a]+'').hide();
                    //    }
                    track_event();
                   assessmentScore(score_details);
            }


        });


    }
    return checkInputs = false;
}
}); 
// $('.download-button').on('click', function(){
//     $('img').hide();
//     $('.budget-list-wrapper h2').hide();
//     $('.budget-list-wrapper h3').show();
//     $('.content-text-box.list').hide();
//     $('.content-text-box.list, .case-study-btn, .download-wrapper .download-button').hide();
//     $('.social-media img').show();
//     setTimeout(function(){ 
//         html2canvas(document.getElementById('dom-to-print'), {
//             windowHeight ,
//         onrendered: function (canvasObj) {
//            startPrintProcess(canvasObj, 'VCN', function () {
//             $('img').show();
//             $('.budget-list-wrapper h2').show();
//             $('.budget-list-wrapper h3').hide();
//             $('.content-text-box.list').show();
//             $('.content-text-box.list, .case-study-btn, .download-wrapper .download-button').show();
//             });   
           
//         }
//     });
//     }, 3000);
// });

$('.download-button').on('click', function(){
    $('.loder_show_after_form_submit').show();
    var htmlcontent = $('#EmailTemplate').html();
    $('#getHtml').html(htmlcontent);


   var a = $('.result-container .chart').html();
   $('#getHtml .chart').html(a);
   var  b = $('.result-container .result-score').html();
   $('#getHtml .result-score').html(b);
var getData = [];
   $('.result-container .two-block-wrapper').each(function(){
    var c = $(this).find('.two-block.grey').html();
    getData.push(c);
   });
   for(var i = 0; i < getData.length; i++){
    $('#getHtml .two-block-wrapper').eq(i).find('.two-block.grey').html(getData[i]);
   }
  

   $('#getHtml .fill').each(function(){
       var a = $(this).attr('data-width');
      if(a == undefined || parseInt(a) <= 0 ){
          $(this).remove();
      }

});
$('#getHtml .fill-diff').each(function(){
    var a = $(this).attr('data-width');
    if(a == undefined || parseInt(a) <= 0 ){
        $(this).remove();
    }

});


    var html2 = $('#getHtml').html();
    
    // console.log(html2);

    setTimeout(function(){ 


        // console.log(html2);

 html2pdf(html2, {
  margin: 0,
  filename: 'VCN_Summry_Report.pdf',
  image: { type: 'jpeg' },
  html2canvas:  { enableRetinaScaling: false, dpi:96 },
  pagebreak:  { after : '.break-page', before:'.before-break' },
  jsPDF:        { unit: 'pt', format: 'letter', orientation: 'portrait' }
}).then(function(pdf) {
    $('.loder_show_after_form_submit').hide();
    $('#getHtml').html('');
});

    }, 1000);
});

$(document).on('click', '.btn.prev', function(){

    if($(this).parents('.main-wrapper').prev('.main-wrapper').hasClass('empty')){
        $('.progress-bar').css('margin',' 0.25vw 0 3vw');
    }

    backClick = true;
    // backArr = sumVar;


    if($(this).parents('.main-wrapper.field').prev('.field').find('.circle.active').length == 4){
        checkInputs = true;
        // $(this).parents('.main-wrapper.field').prev('.field').find('.circle.active').each(function(){
        //     var ansVal = $(this).parents('.circle-container').index()+1;
        //     var colVal = $(this).parents('.question-content').index();
        //     sumVar[colVal] = ansVal;
         
        //     totalSum = sumVar.reduce(getSum);
        //     calLen = (totalSum/20)*100;
        // });


        // var a = $(this).parents('.main-wrapper.field').find('.circle.active').length;
        // barWidth = $(this).parents('.container').find('.progress-bar .fill .handle p').text();
        // barWidthVal = barWidth.split('%');
        // $(this).parents('.container').find('.progress-bar .fill').width(parseInt(barWidthVal)-(2*a)+'%');
        // $(this).parents('.container').find('.progress-bar .fill .handle p').text(parseInt(barWidthVal)-(2*a)+'%');

    }
    // if($(this).parents('.main-wrapper').prev('.decSection').find('.circle.active').length == 5){
    //      $(this).parents('.container').find('.progress-bar .fill').width('25%');
    //      $(this).parents('.container').find('.progress-bar .fill .handle p').text('25%');
    //  }
     if($(this).parents('.main-wrapper').prev('.filter').find('.question-content h4.active').length > 0){
        //  var a =  $(this).parents('.main-wrapper').prev('.filter').attr('data-progress');
        // $(this).parents('.container').find('.progress-bar .fill').width(a);
        // $(this).parents('.container').find('.progress-bar .fill .handle p').text(a);
        filterApplied = true;
    }
    $(this).parents('.main-wrapper').removeClass('active');

        var current_section_name = $(this).parents('.main-wrapper').attr('data-id');
    if(current_section_name == 'decSection'){
        reset_assessment();
    } 

    if($(this).parents('.main-wrapper').prev('.main-wrapper').length){
        $(this).parents('.main-wrapper').prev('.main-wrapper').addClass('active');
        $(this).parents('.main-wrapper').prev('.main-wrapper').find('.btn.next').addClass('active');
        return checkInputs = true;
    }
    else{
        $(this).parents('.container').removeClass('active');
        $(this).parents('.container').prev('.container').addClass('active');
    }





    return checkInputs = true;
    
  
});

    $('form input, form select').on('focus', function(){
        if($(this).attr('id') == 'email'){
            if (validateEmail(email)) {
                $(this).css('border','1px solid #CED4DA');
            }
        }else{
             $(this).css('border','1px solid #CED4DA');
         }
      });
    $('.mail').keyup(function(){
        var email = $(".mail").val();
        if (validateEmail(email)) {
            mailVal = true;
            $('.mail').css('border-color','#CED4DA');
          } else {
            mailVal = false;
            $('.mail').css('border-color','red');
          }
    });
    
    $('.numberonly').keydown(function(event) {

        if(event.shiftKey && ((event.keyCode >=48 && event.keyCode <=57) 
        || (event.keyCode >=186 && event.keyCode <=222))){
        // Ensure that it is a number and stop the Special chars
        event.preventDefault();
        }
        else if ((event.shiftKey || event.ctrlKey) && (event.keyCode > 34 && event.keyCode < 40)){ 
        // let it happen, don't do anything
        } 
        else{
        // Allow only backspace , delete, numbers 
        if (event.keyCode == 9 || event.keyCode == 46 || event.keyCode == 8 || event.keyCode == 39 ||event.keyCode == 37 
        || (event.keyCode >=48 && event.keyCode <=57)) {
        // let it happen, don't do anything
        }
        else {
        // Ensure that it is a number and stop the key press
        event.preventDefault(); 
        }
        }
        });

    $( ".txtOnly" ).keyup(function(e) {
        var key = e.keyCode;
        if (key >= 48 && key <= 57) {
            e.preventDefault();
        }
    });
    var selectOption = false;
   
    $('select').on('change', function(){
        $('select').each(function(){
            // console.log($(this).val());
            
            if($(this).val() == -1 || $(this).val() == ''){
                selectOption == false;
                $(this).parents('.main-wrapper').find('.btn.next').removeClass('active');
                    checkInputs = false;
            }
            else{
                
                selectOption = true;
                if ((!$emptyFields.length) && mailVal == true && selectOption == true) {
                    $(this).parents('.main-wrapper').find('.btn.next').addClass('active');
                    checkInputs = true;  
                }
            }
        });

       
    });


    $('form input').on('keyup', function(){
           $('form input').each(function(){
             $emptyFields = $('form input').filter(function() {
                return $.trim(this.value) === "";
                
            });

            if ((!$emptyFields.length) && mailVal == true && selectOption == true) {
                $(this).parents('.main-wrapper').find('.btn.next').addClass('active');
                checkInputs = true;  
            }
            else{
                $(this).parents('.main-wrapper').find('.btn.next').removeClass('active');
                checkInputs = false;
            }
           });
    }); 

});
// function fillUpdate(){
//     $('.main-wrapper').each(function(){
//         if($(this).is(':visible')){
//             var a = $(this).index();
//         }
//     });
// }
function alphabets_validation(value){	
    var regex = /^[a-zA-Z ]{1,30}$/;
    return regex.test(value);
    }
function getSum(total, num) {
    return total + num;
}
function validateEmail(email) {
    var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(String(email).toLowerCase());
}


// $(window).on('load',function(){
//     $('svg g').addClass('animated fadeIn');
//     $('svg g').each(function(){
//         var count = $(this).index();
//         if(count > 0){
//             $(this).attr('style','animation-delay:'+count/250+'s');
//         }
// });
// $(document).mousemove(function(){
//     var x = event.clientX;
//     var y = event.clientY;
//     $('svg').attr('style','transform:scaleX('+x/1000+') scaleY('+y/1000+')');
// });
// });


// Pdf generation code

function startPrintProcess(canvasObj, fileName, callback) {
    var pdf = new jsPDF('p', 'pt', 'letter');
    pdf.internal.scaleFactor = 3;
        pdfConf = {
            background: '#fff',
            pagesplit:true,
        };
		 document.body.appendChild(canvasObj); //appendChild is required for html to add page in pdf
        pdf.addHTML(canvasObj, 0, 0, pdfConf, function () {
        // pdf.addPage();
		 document.body.removeChild(canvasObj);
        pdf.save(fileName + '.pdf');
        callback();
    });
}

function hideResultBar(index){
    $(index).find('.bar .fill').show();
    $(index).find('.bar .fill-diff').show();
    for(var a=0;a<hideSectionArray.length;a++){
        if($(index).attr('data-id') == hideSectionArray[a]){
            $(index).find('.bar .fill').hide();
            $(index).find('.bar .fill-diff').hide();
        }
       }
}


// sort function callback
function sort_li(a, b){
    return ($(b).data('sort')) < ($(a).data('sort')) ? 1 : -1;    
}

function getKenshooScript(){
kenshoo.trackConversion('4056','664bfeed-cd26-4736-97cf-8071d5dd3b85',{
/*OPTIONAL PARAMETERS. FILL VALUES OR REMOVE UNNEEDED PARAMETERS*/
conversionType: '6260', /*specific conversion type. example: type:'AppInstall' default is 'conv'*/
revenue: 0.0, /*numeric conversion value. example convValue: 12.34*/
currency:'USD', /*example currency:'USD'*/
orderId:'',/*example orderId: 'abc'*/
promoCode:'',
customParam1:'', /*any custom parameter. example: Airport: 'JFK'*/
customParam2:'', /*any custom parameter. example: Rooms: '3'*/
customParamN:'' })
}